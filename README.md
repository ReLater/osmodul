# OSModul
OSModul is a simple module to display a map from [OpenStreetmap](https://openstreetmap.org) on your Joomla website. More information on the [Joomla Extensions Directory](https://extensions.joomla.org/profile/extension/maps-a-weather/maps-a-locations/osmodul)

## Features
- Easy to use
- Set markers to the map
- Multiple marker support
- Marker popup
- Languages: EN, DE, FR, NL, PL
- Custom marker support
- Based on [Leaflet](https://leafletjs.com)
- Different maps on one site
- No ads
- Different map styles
- Custom tile server support
- Auto update
- Moduleclass suffix
- Map scale

## Demo usage
- [http://hof-ronig.de](http://hof-ronig.de)
- [https://www.sailcom.ch](https://www.sailcom.ch/de/flotte/bootsstandorte)

## Help
For help and more information, take a look to the [Wiki](https://gitlab.com/schlumpf/osmodul/wikis/home).
